To replicate the issue with reset trigger 

Activate Environment

```
cd gdk/software
./run_docker_jupyter.sh
source sdk/activate.sh

```
Navigate to "sdk/tests/nn/"

```
cd sdk/tests/nn/

git clone https://[username]@bitbucket.org/gml-hand-detector/hand_gesture_sim.git

```

Run the jupter notebook and check run_simulation.ipynb

```
cd /gdk/sdk/tests/nn/hand_gesture_sim/
jupyter lab --no-browser 

```
"add_integration_layer = 1" is added to last conv layer in "hand_gesture_sim/config_2.ini"

Mapping output can be found in the folder "swipe_model2_4_32_frames_int8_config_2"

Can find integration trigger "top.int_trigger_net.conv2d_2_integration_int_trig_net.integration_trigger"
*Cannot find the reset trigger*

"swipe_model2_4_32_frames_int8_config_2/mapping_output/csv/neurons_names.txt" 

